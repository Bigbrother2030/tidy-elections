---
layout: election
title: "2019 Senegalese presidential election"
country: Senegal
country_code: sn
date: 2019-02-24
wikipedia: https://en.wikipedia.org/wiki/2019_Senegalese_presidential_election
source: https://conseilconstitutionnel.sn/decision-n-4-e-2019-affaire-n-25-e-19/,http://conseilconstitutionnel.sn/wp-content/uploads/2019/03/00_NATIONAL.pdf
maxpercentage: 100
handicap: 1
race: both
candidates:
- name: Macky Sall
  name_short: Sall
  party: APR
  color: "#5C482F"
- name: Idrissa Seck
  name_short: Seck
  party: Rewmi
  color: "#E68F3F"
- name: Ousmane Sonko
  name_short: Sonko
  party: PASTEF
  color: "#CC2B3A"
---
