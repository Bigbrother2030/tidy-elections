var data = [
  {
    "Area": "Costa Rica",
    "PAC": 1322908,
    "PREN": 860388,
    "PAC_percent": 0.606,
    "PREN_percent": 0.394,
    "Winner": "PAC"
  },
  {
    "Area": "Alajuela",
    "PAC": 256282,
    "PREN": 172517,
    "PAC_percent": 0.598,
    "PREN_percent": 0.402,
    "Winner": "PAC"
  },
  {
    "Area": "Cartago",
    "PAC": 211386,
    "PREN": 71879,
    "PAC_percent": 0.746,
    "PREN_percent": 0.254,
    "Winner": "PAC"
  },
  {
    "Area": "Guanacaste",
    "PAC": 82596,
    "PREN": 58403,
    "PAC_percent": 0.586,
    "PREN_percent": 0.414,
    "Winner": "PAC"
  },
  {
    "Area": "Heredia",
    "PAC": 162041,
    "PREN": 78819,
    "PAC_percent": 0.673,
    "PREN_percent": 0.327,
    "Winner": "PAC"
  },
  {
    "Area": "Limón",
    "PAC": 55277,
    "PREN": 95599,
    "PAC_percent": 0.366,
    "PREN_percent": 0.634,
    "Winner": "PREN"
  },
  {
    "Area": "Puntarenas",
    "PAC": 77755,
    "PREN": 94869,
    "PAC_percent": 0.45,
    "PREN_percent": 0.55,
    "Winner": "PREN"
  },
  {
    "Area": "San José",
    "PAC": 473864,
    "PREN": 287233,
    "PAC_percent": 0.623,
    "PREN_percent": 0.377,
    "Winner": "PAC"
  }
]