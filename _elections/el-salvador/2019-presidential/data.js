var data = [
  {
    "Area": "El Salvador",
    "Bukele_percent": 0.531,
    "Calleja_percent": 0.317,
    "Martínez_percent": 0.144,
    "Bukele": 1431723,
    "Calleja": 856919,
    "Martínez": 388947,
    "Winner": "Bukele"
  },
  {
    "Area": "Ahuachapán",
    "Bukele_percent": 0.431,
    "Calleja_percent": 0.361,
    "Martínez_percent": 0.204,
    "Bukele": 59689,
    "Calleja": 50051,
    "Martínez": 28257,
    "Winner": "Bukele"
  },
  {
    "Area": "Cabañas",
    "Bukele_percent": 0.472,
    "Calleja_percent": 0.396,
    "Martínez_percent": 0.129,
    "Bukele": 31346,
    "Calleja": 26325,
    "Martínez": 8586,
    "Winner": "Bukele"
  },
  {
    "Area": "Chalatenango",
    "Bukele_percent": 0.449,
    "Calleja_percent": 0.324,
    "Martínez_percent": 0.223,
    "Bukele": 42092,
    "Calleja": 30364,
    "Martínez": 20934,
    "Winner": "Bukele"
  },
  {
    "Area": "Cuscatlán",
    "Bukele_percent": 0.501,
    "Calleja_percent": 0.339,
    "Martínez_percent": 0.155,
    "Bukele": 57795,
    "Calleja": 39098,
    "Martínez": 17882,
    "Winner": "Bukele"
  },
  {
    "Area": "La Libertad",
    "Bukele_percent": 0.518,
    "Calleja_percent": 0.373,
    "Martínez_percent": 0.099,
    "Bukele": 177832,
    "Calleja": 127868,
    "Martínez": 33823,
    "Winner": "Bukele"
  },
  {
    "Area": "La Paz",
    "Bukele_percent": 0.576,
    "Calleja_percent": 0.294,
    "Martínez_percent": 0.125,
    "Bukele": 79803,
    "Calleja": 40762,
    "Martínez": 17357,
    "Winner": "Bukele"
  },
  {
    "Area": "La Unión",
    "Bukele_percent": 0.545,
    "Calleja_percent": 0.318,
    "Martínez_percent": 0.134,
    "Bukele": 49871,
    "Calleja": 29138,
    "Martínez": 12256,
    "Winner": "Bukele"
  },
  {
    "Area": "Morazán",
    "Bukele_percent": 0.391,
    "Calleja_percent": 0.321,
    "Martínez_percent": 0.285,
    "Bukele": 31649,
    "Calleja": 26007,
    "Martínez": 23102,
    "Winner": "Bukele"
  },
  {
    "Area": "San Miguel",
    "Bukele_percent": 0.543,
    "Calleja_percent": 0.244,
    "Martínez_percent": 0.208,
    "Bukele": 98064,
    "Calleja": 43960,
    "Martínez": 37529,
    "Winner": "Bukele"
  },
  {
    "Area": "San Salvador",
    "Bukele_percent": 0.583,
    "Calleja_percent": 0.3,
    "Martínez_percent": 0.105,
    "Bukele": 479991,
    "Calleja": 246792,
    "Martínez": 86656,
    "Winner": "Bukele"
  },
  {
    "Area": "San Vicente",
    "Bukele_percent": 0.464,
    "Calleja_percent": 0.313,
    "Martínez_percent": 0.219,
    "Bukele": 33765,
    "Calleja": 22786,
    "Martínez": 15921,
    "Winner": "Bukele"
  },
  {
    "Area": "Santa Ana",
    "Bukele_percent": 0.543,
    "Calleja_percent": 0.341,
    "Martínez_percent": 0.109,
    "Bukele": 123413,
    "Calleja": 77550,
    "Martínez": 24695,
    "Winner": "Bukele"
  },
  {
    "Area": "Sonsonate",
    "Bukele_percent": 0.529,
    "Calleja_percent": 0.316,
    "Martínez_percent": 0.149,
    "Bukele": 101794,
    "Calleja": 60796,
    "Martínez": 28599,
    "Winner": "Bukele"
  },
  {
    "Area": "Usulután",
    "Bukele_percent": 0.483,
    "Calleja_percent": 0.265,
    "Martínez_percent": 0.249,
    "Bukele": 64619,
    "Calleja": 35422,
    "Martínez": 33350,
    "Winner": "Bukele"
  }
]