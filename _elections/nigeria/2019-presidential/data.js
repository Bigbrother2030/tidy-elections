var data = [
 {
   "Area": "Nigeria",
   "Winner": "Buhari",
   "Buhari": 15191847,
   "Buhari_percent": 0.57,
   "Atiku": 11262978,
   "Atiku_percent": 0.422
 },
 {
   "Area": "Abia",
   "Winner": "Atiku",
   "Buhari": 85058,
   "Buhari_percent": 0.277,
   "Atiku": 219698,
   "Atiku_percent": 0.717
 },
 {
   "Area": "Adamawa",
   "Winner": "Atiku",
   "Buhari": 378078,
   "Buhari_percent": 0.476,
   "Atiku": 410266,
   "Atiku_percent": 0.517
 },
 {
   "Area": "Akwa Ibom",
   "Winner": "Atiku",
   "Buhari": 175429,
   "Buhari_percent": 0.307,
   "Atiku": 395832,
   "Atiku_percent": 0.692
 },
 {
   "Area": "Anambra",
   "Winner": "Atiku",
   "Buhari": 33298,
   "Buhari_percent": 0.059,
   "Atiku": 524738,
   "Atiku_percent": 0.931
 },
 {
   "Area": "Bauchi",
   "Winner": "Buhari",
   "Buhari": 798428,
   "Buhari_percent": 0.791,
   "Atiku": 209313,
   "Atiku_percent": 0.207
 },
 {
   "Area": "Bayelsa",
   "Winner": "Atiku",
   "Buhari": 118821,
   "Buhari_percent": 0.373,
   "Atiku": 197933,
   "Atiku_percent": 0.622
 },
 {
   "Area": "Benue",
   "Winner": "Atiku",
   "Buhari": 347668,
   "Buhari_percent": 0.489,
   "Atiku": 356817,
   "Atiku_percent": 0.502
 },
 {
   "Area": "Borno",
   "Winner": "Buhari",
   "Buhari": 836496,
   "Buhari_percent": 0.92,
   "Atiku": 71788,
   "Atiku_percent": 0.079
 },
 {
   "Area": "Cross River",
   "Winner": "Atiku",
   "Buhari": 117302,
   "Buhari_percent": 0.282,
   "Atiku": 295737,
   "Atiku_percent": 0.712
 },
 {
   "Area": "Delta",
   "Winner": "Atiku",
   "Buhari": 221292,
   "Buhari_percent": 0.27,
   "Atiku": 594068,
   "Atiku_percent": 0.724
 },
 {
   "Area": "Ebonyi",
   "Winner": "Atiku",
   "Buhari": 90726,
   "Buhari_percent": 0.258,
   "Atiku": 258573,
   "Atiku_percent": 0.737
 },
 {
   "Area": "Edo",
   "Winner": "Atiku",
   "Buhari": 267842,
   "Buhari_percent": 0.488,
   "Atiku": 275691,
   "Atiku_percent": 0.503
 },
 {
   "Area": "Ekiti",
   "Winner": "Buhari",
   "Buhari": 219231,
   "Buhari_percent": 0.586,
   "Atiku": 154032,
   "Atiku_percent": 0.412
 },
 {
   "Area": "Enugu",
   "Winner": "Atiku",
   "Buhari": 54423,
   "Buhari_percent": 0.132,
   "Atiku": 355553,
   "Atiku_percent": 0.863
 },
 {
   "Area": "Federal Capital Territory",
   "Winner": "Atiku",
   "Buhari": 152224,
   "Buhari_percent": 0.367,
   "Atiku": 259997,
   "Atiku_percent": 0.626
 },
 {
   "Area": "Gombe",
   "Winner": "Buhari",
   "Buhari": 402961,
   "Buhari_percent": 0.743,
   "Atiku": 138484,
   "Atiku_percent": 0.255
 },
 {
   "Area": "Imo",
   "Winner": "Atiku",
   "Buhari": 140463,
   "Buhari_percent": 0.294,
   "Atiku": 334923,
   "Atiku_percent": 0.701
 },
 {
   "Area": "Jigawa",
   "Winner": "Buhari",
   "Buhari": 794738,
   "Buhari_percent": 0.729,
   "Atiku": 289895,
   "Atiku_percent": 0.266
 },
 {
   "Area": "Kaduna",
   "Winner": "Buhari",
   "Buhari": 993445,
   "Buhari_percent": 0.604,
   "Atiku": 649612,
   "Atiku_percent": 0.395
 },
 {
   "Area": "Kano",
   "Winner": "Buhari",
   "Buhari": 1464768,
   "Buhari_percent": 0.788,
   "Atiku": 391593,
   "Atiku_percent": 0.211
 },
 {
   "Area": "Katsina",
   "Winner": "Buhari",
   "Buhari": 1232133,
   "Buhari_percent": 0.8,
   "Atiku": 308056,
   "Atiku_percent": 0.2
 },
 {
   "Area": "Kebbi",
   "Winner": "Buhari",
   "Buhari": 581552,
   "Buhari_percent": 0.787,
   "Atiku": 154282,
   "Atiku_percent": 0.209
 },
 {
   "Area": "Kogi",
   "Winner": "Buhari",
   "Buhari": 285894,
   "Buhari_percent": 0.559,
   "Atiku": 218207,
   "Atiku_percent": 0.427
 },
 {
   "Area": "Kwara",
   "Winner": "Buhari",
   "Buhari": 308984,
   "Buhari_percent": 0.688,
   "Atiku": 138184,
   "Atiku_percent": 0.308
 },
 {
   "Area": "Lagos",
   "Winner": "Buhari",
   "Buhari": 580825,
   "Buhari_percent": 0.551,
   "Atiku": 448015,
   "Atiku_percent": 0.425
 },
 {
   "Area": "Nassarawa",
   "Winner": "Buhari",
   "Buhari": 289903,
   "Buhari_percent": 0.505,
   "Atiku": 283847,
   "Atiku_percent": 0.494
 },
 {
   "Area": "Niger",
   "Winner": "Buhari",
   "Buhari": 612371,
   "Buhari_percent": 0.736,
   "Atiku": 218052,
   "Atiku_percent": 0.262
 },
 {
   "Area": "Ogun",
   "Winner": "Buhari",
   "Buhari": 281762,
   "Buhari_percent": 0.554,
   "Atiku": 194655,
   "Atiku_percent": 0.383
 },
 {
   "Area": "Ondo",
   "Winner": "Atiku",
   "Buhari": 241769,
   "Buhari_percent": 0.455,
   "Atiku": 275901,
   "Atiku_percent": 0.519
 },
 {
   "Area": "Osun",
   "Winner": "Buhari",
   "Buhari": 347634,
   "Buhari_percent": 0.505,
   "Atiku": 337377,
   "Atiku_percent": 0.49
 },
 {
   "Area": "Oyo",
   "Winner": "Atiku",
   "Buhari": 365229,
   "Buhari_percent": 0.468,
   "Atiku": 366690,
   "Atiku_percent": 0.469
 },
 {
   "Area": "Plateau",
   "Winner": "Atiku",
   "Buhari": 468555,
   "Buhari_percent": 0.459,
   "Atiku": 548665,
   "Atiku_percent": 0.538
 },
 {
   "Area": "Rivers",
   "Winner": "Atiku",
   "Buhari": 150710,
   "Buhari_percent": 0.24,
   "Atiku": 473971,
   "Atiku_percent": 0.755
 },
 {
   "Area": "Sokoto",
   "Winner": "Buhari",
   "Buhari": 490333,
   "Buhari_percent": 0.575,
   "Atiku": 361604,
   "Atiku_percent": 0.424
 },
 {
   "Area": "Taraba",
   "Winner": "Atiku",
   "Buhari": 324906,
   "Buhari_percent": 0.464,
   "Atiku": 374743,
   "Atiku_percent": 0.535
 },
 {
   "Area": "Yobe",
   "Winner": "Buhari",
   "Buhari": 497914,
   "Buhari_percent": 0.907,
   "Atiku": 50763,
   "Atiku_percent": 0.092
 },
 {
   "Area": "Zamfara",
   "Winner": "Buhari",
   "Buhari": 438682,
   "Buhari_percent": 0.777,
   "Atiku": 125423,
   "Atiku_percent": 0.222
 },
]