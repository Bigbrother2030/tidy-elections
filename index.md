---
layout: base
class: home
menu: false
---
<div class="ui top attached segment masthead">
	<i class="vote yea icon"></i>
	<h1 class="ui header">Tidy Elections</h1>
	<p class="explainer">Clean, freely available election result maps and data of relatively little cared about elections</p>
  <a href="elections.html" class="ui large primary labeled icon button"><i class="person booth icon"></i>Explore elections</a>
  <a href="about.html" class="ui large grey labeled icon button"><i class="info circle icon"></i>About</a>
</div>
<div class="ui attached segment equal width stackable grid features">
	<div class="column">
		<i class="huge map outline icon"></i>
		<h2>Clean maps</h2>
		<p>Each election has a clean, no-frills map through which you can explore the election results. Basemaps are downloadable in SVG format and contain the names of each region.</p>
	</div>
	<div class="column">
		<i class="huge table icon"></i>
		<h2>Organised data</h2>
		<p>All election result data has been cleaned and is available in the CSV format, ready to be used for your own application.</p>
	</div>
	<div class="column">
		<i class="huge creative commons icon"></i>
		<i class="huge creative commons by icon"></i>
		<i class="huge creative commons sa icon"></i>
		<h2>Free and permissive</h2>
		<p>Tidy Elections is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International License</a>. <strong>You are free to use, share, and adapt it for any purpose</strong>. Please read the license for more details.</p>
	</div>
</div>