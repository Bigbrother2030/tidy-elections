// make menu dropdowns work
$(".menu .dropdown").dropdown();

// open and close sidebar on mobile
$(".toggle-sidebar").click(function() {
  $("nav").toggleClass("negative-margin");
  $(".horizontal-menu-overlay").fadeToggle();
});