---
title: "2019 Beninese parliamentary election"
country: Benin
date: 2019-04-28
country_code: bj
wikipedia: https://en.wikipedia.org/wiki/2019_Beninese_parliamentary_election
discussion: https://gitlab.com/guushoekman/tidy-elections/issues
---