---
layout: base
class: about
title: About
---
<div class="ui attached segment about-te">
	<div class="content">
		<h2>About Tidy Elections</h2>
		<p>I (<a href="https://guushoekman.com">Guus Hoekman</a>) started Tidy Elections after being unpleasantly surprised by how difficult it is to find clean data of elections held outside wealthy and/or "important" countries. Tidy Elections is an attempt to fill that gap.</p>
		<h2>Goals</h2>
		<p>I can see Tidy Elections being useful for researchers, academics, journalists, and really anyone with an interest in one of the covered elections. For them, I'd like to provide:</p>
		<ul>
			<li>Clean and open election data on a subnational level, especially for elections for which this doesn't already exist</li>
			<li>A simple way to explore this data</li>
			<li>Open source code that allows anyone to recreate, tweak, and improve Tidy Elections</li>
			<li>A project that is collaborative in nature</li>
		</ul>
		<p>In light of that last point: contributions are most welcome! Read how on the <a href="https://gitlab.com/guushoekman/tidy-elections/blob/master/CONTRIBUTING.md">contribution page</a>.</p>
		<h2>A word of warning</h2>
		<p><strong>Don't assume the data on Tidy Elections is accurate!</strong> There is no reason you should blindly assume the data on here is correct.</p>
		<p>That being said, Tidy Elections has been made with only good intentions and any errors there might be were made accidentally. If you have any doubts, please <a href="https://gitlab.com/guushoekman/tidy-elections">check out the source code</a> and/or <a href="https://gitlab.com/guushoekman/tidy-elections/tree/master/_elections">verify the data against the original sources</a>.</p>
		<p>If you find any errors, <a href="https://gitlab.com/guushoekman/tidy-elections/issues">please report them</a>.</p>
		<h2>Attributions</h2>
		<ul>
			<li>Layout: <a href="https://fomantic-ui.com/">Fomantic UI</a></li>
			<li>Icons: <a href="http://fontawesome.com/">Font Awesome</a></li>
			<li>Hosting: <a href="https://gitlab.com/guushoekman/tidy-elections">GitLab</a></li>
		</ul>
	</div>
</div>
